@ECHO off
color 0A
set MVN_EXE=C:\app\apache-maven-3.6.0\bin\mvn
set JAVA_HOME=C:\app\java\jdk1.8.0_181


REM set path=%path%;C:\APPS\apache-maven-3.3.9\bin
set JAR_NAME=ojdbc6.jar
SET GROUP_ID=com.oracle
SET ARTIFACT_ID=ojdbc6
SET VERSION=11.2.0

call %MVN_EXE% install:install-file -Dfile=%JAR_NAME% -DgroupId=%GROUP_ID% -DartifactId=%ARTIFACT_ID% -Dversion=%VERSION% -Dpackaging=jar
pause
