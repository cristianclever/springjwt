package br.com.santander.sample.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.santander.sample.dto.AuthenticationResult;
import br.com.santander.sample.service.TokenAuthenticationService;

@RequestMapping("/login")
@RestController()
public class LoginController extends BaseController {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(LoginController.class);

	
	private TokenAuthenticationService tokenAuthenticationService;
	
	
	@Autowired
	public LoginController(TokenAuthenticationService tokenAuthenticationService) {
		this.tokenAuthenticationService= tokenAuthenticationService;
	}
	
	

	@PostMapping("/")
	public ResponseEntity<AuthenticationResult> getToken(@RequestParam("username") String username, @RequestParam("password") String password) {
		
		AuthenticationResult result  = tokenAuthenticationService.getAuthenticationToken(username, password);
		
		if(result.isResult()) {
			return  new ResponseEntity<AuthenticationResult>(result,HttpStatus.ACCEPTED);
		}
		
		result = null;
		return  new ResponseEntity<AuthenticationResult>(result,HttpStatus.FORBIDDEN);
	}
	
}
