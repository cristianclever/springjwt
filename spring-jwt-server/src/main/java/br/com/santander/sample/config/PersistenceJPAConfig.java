package br.com.santander.sample.config;

import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import br.com.santander.sample.advice.ControllerExceptionHandler;
import br.com.santander.sample.model.BaseModel;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories("br.com.santander.sample.repository")
public class PersistenceJPAConfig {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(ControllerExceptionHandler.class);

	@PostConstruct
	public void init() {
		log.info(this.getClass().getName() + " [init]");
	}

	@Autowired
	private DataSource ds;

	@Autowired
	private ApplicationContext context;

	private static Logger logger = LogManager.getLogger(PersistenceJPAConfig.class);

	private static final String[] jpaPackages = {
			BaseModel.class.getPackage().getName() /* "br.com.santander.sample.model" */ };

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(ds);
		em.setPackagesToScan(jpaPackages);
		JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);

		Properties hibernateProperties = new Properties();

		// em.setJpaProperties(hibernateProperties);
		em.afterPropertiesSet();

		return em;

	}

	@Bean
	public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(emf);
		return transactionManager;
	}

	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
		return new PersistenceExceptionTranslationPostProcessor();
	}

}
