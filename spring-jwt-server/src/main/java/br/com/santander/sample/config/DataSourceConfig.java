package br.com.santander.sample.config;

import javax.sql.DataSource;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import br.com.santander.sample.advice.ControllerExceptionHandler;

@Configuration

public class DataSourceConfig {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(ControllerExceptionHandler.class);

	
	@Bean
	DataSource createDatasource() {
		
		HikariConfig hikariConfig = getHikariConfigProperties();
		HikariDataSource ds = new HikariDataSource(hikariConfig);
		
		//spring.datasource.hikari.minimumIdle=5
		//spring.datasource.hikari.maximumPoolSize=8

		return ds;
	}	
	
	

	@Bean
	@ConfigurationProperties(prefix = "datasource.hikari")
	public HikariConfig getHikariConfigProperties() {
		HikariConfig hikariConfig = new HikariConfig();
		
		return hikariConfig;
	}
	


}
