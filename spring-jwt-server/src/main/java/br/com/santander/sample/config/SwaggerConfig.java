package br.com.santander.sample.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.santander.sample.advice.ControllerExceptionHandler;
import br.com.santander.sample.controller.BaseController;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {      
	
	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(ControllerExceptionHandler.class);

	
	private static final String BASE_PACKAGE  = BaseController.class.getPackage().getName();
	
    @Bean
    public Docket api() { 
    	
    	
    	log.info("SwaggerConfig.api()");
    	
        return new Docket(DocumentationType.SWAGGER_2)  
        		
          .select()                                  

          .apis(RequestHandlerSelectors.basePackage( BASE_PACKAGE))

          .paths(PathSelectors.any())                  
          .build();                                           
    }
    

}
