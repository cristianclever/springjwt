package br.com.santander.sample.service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;

import javax.cache.annotation.CacheResult;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import br.com.santander.sample.dto.AuthenticationResult;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class TokenAuthenticationService extends BaseTransactionService {

	@Value("${expirationTime}")
	public long expirationTime;

	@Value("${secretKey}")
	private String secretKey;

	
	private Date getDateFromLocalDateTime(LocalDateTime ldt) {
		Date dt = Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant() );
		return dt;
	}
	
	
	@CacheResult(cacheName="defaultCache")
	public AuthenticationResult getAuthenticationToken(String username, String password) {

		AuthenticationResult auth = new AuthenticationResult();

		boolean isValid = validateUser(username, password);

		
		if (isValid) {
			LocalDateTime ldt = LocalDateTime.now().plusSeconds(expirationTime);
			Date exp = getDateFromLocalDateTime(ldt);
			Optional<String> result =  getAuthenticationToken(username, 1L, "Cristian Clever", "admin",exp);
			result.ifPresent(r-> {
				auth.setToken(r);
				auth.setResult(true);
				auth.setExpiration(exp);
			});
			
		}

		return auth;
	}

	
	
	public boolean validateUser(String username, String password) {
		String user = "cristian";
		return user.equals(username);
		
	}

	public Optional<String> getAuthenticationToken(String username, long idUsuario, String nomeUsuario, String role,Date exp) {


		Claims claims = Jwts.claims().setSubject(username);
		claims.put("idUsuario", idUsuario);
		claims.put("nomeUsuario", nomeUsuario);
		claims.put("dtValidadeToken", exp);
		claims.put("perfil", role);

		String jwt = Jwts.builder().setClaims(claims).setSubject(username).setExpiration(exp).signWith(SignatureAlgorithm.HS512, "1234567890").compact();

		
		
		//teste para reverter
		 Claims claims_ = Jwts.parser() .setSigningKey("1234567890") .parseClaimsJws(jwt)
		 .getBody();
		 
		
		
		Optional<String> result = Optional.of(jwt);

		return result;
	}

}
