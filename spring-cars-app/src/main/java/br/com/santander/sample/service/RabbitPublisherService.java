package br.com.santander.sample.service;

import java.util.Date;

import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.santander.sample.controller.RabbitController;
import br.com.santander.sample.dto.Car;

@Service
public class RabbitPublisherService extends BaseTransactionService{

	
	//private MessageConverter messageConverter = new SimpleMessageConverter();
	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(RabbitController.class);
	
	private RabbitTemplate rabbitTemplate;

	@Autowired
	public RabbitPublisherService(RabbitTemplate rabbitTemplate) {
		this.rabbitTemplate = rabbitTemplate;
	}
	
	
	public void publishCar(Car car) {
		
		MessagePostProcessor messagePostProcessor = (m)->{
			MessageProperties messageProperties = m.getMessageProperties();
			messageProperties.setHeader("X_ORDER_SOURCE", "WEB");
			return m;
		};
		
		
		final String routingKey = "tacocloud.order.queue";
		log.info("Creando objeto no babbit:" + car);
		rabbitTemplate.convertAndSend(routingKey,car,messagePostProcessor );
		
		
		
		
	}
	

}
