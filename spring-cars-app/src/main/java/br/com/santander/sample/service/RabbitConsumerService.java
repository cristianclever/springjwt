package br.com.santander.sample.service;

import java.util.Map;

import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.santander.sample.controller.RabbitController;
import br.com.santander.sample.dto.Car;

@Service
public class RabbitConsumerService extends BaseTransactionService {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(RabbitController.class);

	
	private RabbitTemplate rabbitTemplate;
	private MessageConverter converter;

	@Autowired
	public RabbitConsumerService(RabbitTemplate rabbitTemplate) {
		this.rabbitTemplate = rabbitTemplate;
		converter = rabbitTemplate.getMessageConverter();

	}
	
	
/*	
 @RabbitListener(bindings = @QueueBinding(
        value = @Queue(value = "myQueue", durable = "true"),
        exchange = @Exchange(value = "auto.exch"),
        key = "orderRoutingKey")
  )	
*/
	@RabbitListener(queues = "tacocloud.order.queue")
	public void receiveCar(Car car,Message message) {

		//Map<String,Object> mapHeaders =  messageProperties.getHeaders();
		
		log.info("receivedCar:"+car);
		


	}

}
