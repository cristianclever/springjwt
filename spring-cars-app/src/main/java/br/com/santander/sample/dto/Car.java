package br.com.santander.sample.dto;

import java.awt.Color;
import java.io.Serializable;
import java.time.LocalDate;

import javax.validation.constraints.NotEmpty;

public class Car implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	

	private LocalDate anoFabricacao;
	
	private String cor;
	
	
	@NotEmpty
	private String modelo;
	
	private String marca;

	public LocalDate getAnoFabricacao() {
		return anoFabricacao;
	}

	public void setAnoFabricacao(LocalDate anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	@Override
	public String toString() {
		return "Car [anoFabricacao=" + anoFabricacao + ", cor=" + cor + ", modelo=" + modelo + ", marca=" + marca + "]";
	}

}
