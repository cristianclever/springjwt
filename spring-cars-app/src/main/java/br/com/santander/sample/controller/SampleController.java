package br.com.santander.sample.controller;

import java.awt.Color;
import java.time.LocalDate;
import java.time.Month;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.santander.sample.dto.Car;
import br.com.santander.sample.dto.Retorno;
import br.com.santander.sample.service.ParameterService;
import br.com.santander.sample.service.RabbitPublisherService;



@RequestMapping("/test")
@RestController
public class SampleController extends BaseController{
	
	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(SampleController.class);
	
	@Autowired
	public ParameterService parameterService;
	

	
	//@Secured({ "ROLE_VIEWER", "ROLE_EDITOR" , "ROLE_ADMIN"})
	@GetMapping("/x2")
	public Retorno index() {
		
	
		log.info(""+ new Date());
		
		parameterService.findAllParameters();
		
		
		Retorno r = new Retorno();
		//r=null;
		r.setCodigo(1);
		r.setDescricao("Teste");
		
		Retorno r2 = new Retorno();
		r2.setCodigo(12);
		r2.setDescricao("Teste2");	
		
		r.setSubretorno(r2);
		
		return r;
	}



	
}
