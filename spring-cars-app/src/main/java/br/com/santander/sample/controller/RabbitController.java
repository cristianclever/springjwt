package br.com.santander.sample.controller;

import java.awt.Color;
import java.time.LocalDate;
import java.time.Month;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.santander.sample.dto.Car;
import br.com.santander.sample.service.ParameterService;
import br.com.santander.sample.service.RabbitPublisherService;



@RequestMapping("/rabbit")
@RestController
public class RabbitController extends BaseController{
	
	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(RabbitController.class);
	
	@Autowired
	public ParameterService parameterService;
	
	@Autowired
	public RabbitPublisherService rabbitService;
	
	
	
/****	
	
	{
	"anoFabricacao":"2019-02-09T20:24:01.595Z" , -> new Date().toISOString()
	"cor": "Prata",
	"modelo":"Versa",
	"marca":"Nissan"
	}
	
***/	
	@Secured({"ROLE_ADMIN"})
	@PostMapping("/")
	@ResponseStatus(code=HttpStatus.CREATED)
	public void publishCar(@RequestBody @Valid Car c) {
		
		/*
		Car c = new Car();
		c.setAnoFabricacao(LocalDate.of(1977, Month.JULY, 1));
		c.setCor(Color.BLACK);
		c.setMarca("Fiat");
		*/
		
		rabbitService.publishCar(c);
		
	}	
	

}
